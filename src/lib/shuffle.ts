// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
export default function <T>(a: T[]) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const t = a[i];
    a[i] = a[j];
    a[j] = t;
  }
}
