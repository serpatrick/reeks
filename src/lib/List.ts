import wrap from '../utils/wrap.js';
import remove from './remove.js';
import pick from './pick.js';
import shuffle from './shuffle.js';

export default class List<T> {
  list: T[];

  constructor(list: T[] = []) {
    this.list = list;
  }

  static create<T>(length: number, value?: T) {
    const list: T[] = Array(length);

    if (value !== undefined) {
      list.fill(value);
    }

    return new List(list);
  }

  get(index: number) {
    const i = wrap(index, this.list.length);
    return this.list[i];
    return this;
  }

  set(index: number, item: T) {
    const i = wrap(index, this.list.length);
    this.list[i] = item;
    return this;
  }

  add(...items: (T | null | undefined)[]) {
    for (const item of items) {
      if (item === null || item === undefined) {
        continue;
      }
      this.list.push(item);
    }
    return this;
  }

  remove(...items: (T | null | undefined)[]) {
    for (const item of items) {
      if (item === null || item === undefined) {
        continue;
      }
      remove(this.list, item);
    }
    return this;
  }

  pick() {
    return pick(this.list);
  }

  shuffle() {
    shuffle(this.list);
    return this;
  }

  empty() {
    this.list = [];
    return this;
  }

  forEach(callback: (item: T, index: number, list: T[]) => void) {
    this.list.forEach(callback);
    return this;
  }

  map<K>(callback: (item: T, index: number, list: T[]) => K) {
    const list = this.list.map(callback);
    return new List(list);
  }

  find(callback: (item: T, index: number, list: T[]) => boolean) {
    return this.list.find(callback);
  }

  filter(callback: (item: T, index: number, list: T[]) => boolean) {
    const list = this.list.filter(callback);
    return new List(list);
  }

  sort(callback: (a: T, b: T) => number) {
    this.list.sort(callback);
    return this;
  }

  some(callback: (item: T, index: number, list: T[]) => boolean) {
    return this.list.some(callback);
  }

  every(callback: (item: T, index: number, list: T[]) => boolean) {
    return this.list.every(callback);
  }

  includes(item?: T) {
    if (item === undefined) {
      return false;
    }
    return this.list.includes(item);
  }

  first() {
    return this.list[0];
  }

  last() {
    return this.list[this.length - 1];
  }

  values() {
    return this.list.values();
  }

  get length() {
    return this.list.length;
  }
}
