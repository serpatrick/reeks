export default function <T>(a: T[], item?: T) {
  if (item === undefined) {
    return;
  }

  const index = a.indexOf(item);

  if (index > -1) {
    a.splice(index, 1);
  }
}
